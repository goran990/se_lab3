from matrix import Matrix
print("Moj Octave")
print("Verzija 0.0.la")
print("Autor: Goran Mitrovic")

### a = [ 1 2 3 ; 4 5 6 ; 7 8 9 ]
### a = 1 2 3 ; 4 5 6
varijable = {}
while True:
    print("varijable: " + str(varijable))
    txt = raw_input(">> ")
    if "[" in txt and "]" in txt:
        if "=" in txt:
            mat_list = []
            var,mat = txt.split("=")
            mat = mat.strip()
            mat = mat.strip("[")
            mat = mat.strip("]")
            redovi = mat.split(";")
            
            for red in redovi:
                red_list =  []
                elems = red.split()
                
                for el in elems:
                    red_list.append(float(el.strip()))
                mat_list.append(red_list)
                             
                       
            m = Matrix.fromList(mat_list)
            varijable[var.strip()] = m
        
    if "vars" in txt:
        for var in varijable.keys():
            print(var +": "+repr(varijable[var]))

    if txt in varijable:
        print(varijable[txt])
 
    if "+" in txt:
        if "=" in txt:
            var, zbroj= txt.strip().split("=")
            x = zbroj.strip().split("+")
            varijable[var.strip()] = varijable[x[0]].add(varijable[x[1]])  
            print  (varijable[var.strip()])
        else:    
            x = txt.strip().split("+")
            print(varijable[x[0]].add(varijable[x[1]]))
       
    if "-" in txt:
        if "=" in txt:
            var, zbroj= txt.strip().split("=")
            x = zbroj.strip().split("-")
            varijable[var.strip()] = varijable[x[0]].subtract(varijable[x[1]])  
            print  (varijable[var.strip()])
        else:    
            x = txt.strip().split("-")
            print(varijable[x[0]].subtract(varijable[x[1]]))
 
    if "*" in txt:
        if "=" in txt:
            var, zbroj= txt.strip().split("=")
            x = zbroj.strip().split("*")
            varijable[var.strip()] = varijable[x[0]].dotMultiply(varijable[x[1]])  
            print  (varijable[var.strip()])
        else:    
            x = txt.strip().split("*")
            print(varijable[x[0]].dotMultiply(varijable[x[1]]))
 
    if "/" in txt:
        if "=" in txt:
            var, zbroj= txt.strip().split("=")
            x = zbroj.strip().split("/")
            varijable[var.strip()] = varijable[x[0]].dotDivide(varijable[x[1]])
            print  (varijable[var.strip()])
        else:    
            x = txt.strip().split("/")
            print(varijable[x[0]].dotDivide(varijable[x[1]]))

    
                
            

    if txt == "exit":
        break