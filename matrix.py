import unittest

class MatrixError(Exception):
    pass

class Matrix(object):
    def __init__(self, m, n):
        if m<=0 or n<=0:
            raise MatrixError("Dimensions must be positive")
        self.m = m 
        self.n = n
        self.rows = []
        for i in range(m):
            row = [0] * n
            self.rows.append(row)

    def reset(self):
        for row in range(self.m):
            for col in range(self.n):
                self.rows[row][col] = 0
    def add(self, mat):
        if self.m != mat.m or self.n != mat.n:
            raise MatrixError("Cannot add matrices")
        newmat = Matrix(self.m, self.n)
        for row in range(self.m):
            for col in range(self.n):
                newmat[row][col] = self.rows[row][col] + mat[row][col]
        return newmat
    def subtract(self, mat):
        if self.m != mat.m or self.n != mat.n:
            raise MatrixError("Cannot subtract matrices")
        newmat = Matrix(self.m, self.n)
        for row in range(self.m):
            for col in range(self.n):
                newmat[row][col] = self.rows[row][col] - mat[row][col]
        return newmat

    def dotMultiply(self, mat):
        if self.m != mat.m or self.n != mat.n:
            raise MatrixError("Cannot dotMultiply matrices")
        newmat = Matrix(self.m, self.n)
        for row in range(self.m):
            for col in range(self.n):
                newmat[row][col] = self.rows[row][col] * mat[row][col]
        return newmat

    def dotDivide(self, mat):
        if self.m != mat.m or self.n != mat.n:
            raise MatrixError("Cannot dotDivide matrices")
        newmat = Matrix(self.m, self.n)
        for row in range(self.m):
            for col in range(self.n):
                newmat[row][col] = self.rows[row][col] / mat[row][col]
        return newmat
    def avg(self):
        suma = 0.0
        for row in range(self.m):
            for col in range(self.n):
                suma += self.rows[row][col]
        return float(suma) / (self.m*self.n)

    def min(self):
        min = self [0][0]
        for row in range(self.m):
            for col in range(self.n):
                if(min> self.rows[row][col]):
                    min = self.rows[row][col]
        return (min)

    def sum(self):
        suma = 0.0
        for row in range(self.m):
            for col in range(self.n):
                suma += self.rows[row][col]
        return float(suma) 
        
    def max(self):
        max = self [0][0]
        for row in range(self.m):
            for col in range(self.n):
                if(max< self.rows[row][col]):
                    max = self.rows[row][col]
        return (max)

    def __str__(self):
        s = "Matrix "+str(self.m) +"x"+str(self.n) +"\n"
        for row in self.rows:
            for el in row:
                s += str(el) + "\t"
            s += "\n"
        return s
    def __repr__(self):
        return str(self.m)+"x"+str(self.n)

    def __getitem__(self, row):
        return self.rows[row]
    def __setitem__(self, row, col, value):
        self.rows[row][col] = value
    def __eq__(self, mat):
        if self.m != mat.m or self.n != mat.n:
            return False
        for row in range(self.m):
            for col in range(self.n):
                if self.rows[row][col] != mat[row][col]:
                    return False
        return True


    



    def hello(self):
        print ("Hello, I'm matrix dim: " + str(self.m) + "x" + str(self.n))

    @classmethod
    def fromList(cls, listoflists):
        rows = len(listoflists)
        cols = len(listoflists[0])
        m = Matrix(rows, cols)
        for row in range(rows):
            for col in range(cols):
                m[row][col] = listoflists[row][col]
        return m


class MatrixTests(unittest.TestCase):
    def testInit(self):
        m1 = Matrix(3,4)
        self.assertTrue(m1.m == 3)
        self.assertTrue(m1.n == 4)
        m2 = Matrix(2,2)
        m2s = "Matrix 2x2\n"
        m2s += "0\t0\t\n"
        m2s += "0\t0\t\n"
        self.assertTrue(str(m2) == m2s)

    def testDimensions(self):
        with self.assertRaises(MatrixError) as context:
             m1 = Matrix(0,0)
             m2 = Matrix(-3,0)
             m4 = Matrix(0,-3)
             m3 = Matrix(-3,-4)

    def testEq(self):
        m1 = Matrix(2,2)
        m2 = Matrix(2,2)
        m3 = Matrix(2,3)
        self.assertTrue( m1== m2)
        self.assertFalse( m1 == m3)

    def testFromList(self):
        m1 = Matrix(2,2)
        m2 = Matrix.fromList([ [0,0], [0,0] ])
        m3 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        self.assertTrue( m1 == m2 )
        self.assertFalse( m2 == m3 )
        

    def testFromListInconsistentDimensions(self):
        with self.assertRaises(MatrixError) as cobtext:
            m4 = Matrix.fromList( [ [1,2], [3,4,5] ])
            m5 = Matrix.fromList( [ [], [3,4,5] ])
            m6 = Matrix.fromList( [ [1,2], [] ])
            m7 = Matrix.fromList( [1,2])
    def testReset(self):
        m1 = Matrix.fromList([ [1,2], [3,4] ])
        m2 = Matrix.fromList([ [0,0], [0,0] ])
        m1.reset()
        self.assertTrue( m1 == m2 )
    def testAdd(self):
        m1 = Matrix.fromList([ [1,2], [3,4] ])
        m2 = Matrix.fromList([ [2,3], [3,2] ])
        m  = Matrix.fromList([ [3,5], [6,6] ])
        m3 = m1.add(m2)
        self.assertTrue( m3 == m )
        m4 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        with self.assertRaises(MatrixError) as c:
            m5 = m1.add(m4)
    def testAvg(self):
        m1 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        self.assertTrue( m1.avg() == 3.5 )
    
    def testSubtract(self):
        m1 = Matrix.fromList([ [1,2], [3,4] ])
        m2 = Matrix.fromList([ [2,3], [3,2] ])
        m  = Matrix.fromList([ [-1,-1], [0,2] ])
        m3 = m1.subtract(m2)
        self.assertTrue( m3 == m )
        m4 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        with self.assertRaises(MatrixError) as c:
            m5 = m1.subtract(m4)

    def testdotMultiply(self):
        m1 = Matrix.fromList([ [1,2], [3,4] ])
        m2 = Matrix.fromList([ [2,3], [3,2] ])
        m  = Matrix.fromList([ [2,6], [9,8] ])
        m3 = m1.dotMultiply(m2)
        self.assertTrue( m3 == m )
        m4 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        with self.assertRaises(MatrixError) as c:
            m5 = m1.dotMultiply(m4)

    def testdotDivide(self):
        m1 = Matrix.fromList([ [1,2], [3,4] ])
        m2 = Matrix.fromList([ [2,3], [3,2] ])
        m  = Matrix.fromList([ [0,0], [1,2] ])
        m3 = m1.dotDivide(m2)
        self.assertTrue( m3 == m )
        m4 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        with self.assertRaises(MatrixError) as c:
            m5 = m1.dotDivide(m4)

    def testMin(self):
        m1 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        self.assertTrue( m1.min() == 1 )

    def testMax(self):
        m1 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        self.assertTrue( m1.max() == 6 )

    def testSum(self):
        m1 = Matrix.fromList([ [1,2,3], [4,5,6] ])
        self.assertTrue( m1.sum() == 21  )

    




        

if __name__ == "__main__":
    unittest.main()